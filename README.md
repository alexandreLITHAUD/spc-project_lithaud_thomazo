# Compte Rendu Projet SPC - Simon || LITHAUD - THOMAZO
 > Thomazo Corentin - Lithaud Alexandre, avril 2022

**Plan du document**

- [Compte Rendu Projet SPC - Simon || LITHAUD - THOMAZO](#compte-rendu-projet-spc---simon--lithaud---thomazo)
- [État du projet](#état-du-projet)
  - [Matériel utilisé](#matériel-utilisé)
  - [Ce que l'on a réalisé](#ce-que-lon-a-réalisé)
    - [Générateur aléatoire](#générateur-aléatoire)
    - [Initialisation du germe](#initialisation-du-germe)
    - [paramétrage des composants](#paramétrage-des-composants)
  - [Ce que l'on a abandonné](#ce-que-lon-a-abandonné)
    - [Ajout de plus de couleurs](#ajout-de-plus-de-couleurs)
    - [Ajout de son](#ajout-de-son)
  - [Ce que l'on aurait aimé implenter](#ce-que-lon-aurait-aimé-implenter)
    - [Ajout d'un mode multijoueur](#ajout-dun-mode-multijoueur)
    - [Ajout d'un timer pour le temps de réponse](#ajout-dun-timer-pour-le-temps-de-réponse)
    - [Ajout d'un mode de difficulté](#ajout-dun-mode-de-difficulté)
- [Points important de notre projet](#points-important-de-notre-projet)
  - [Les interruptions](#les-interruptions)
    - [Objectif des interruptions système](#objectif-des-interruptions-système)
    - [Comment nous avons réalisé les interruptions système](#comment-nous-avons-réalisé-les-interruptions-système)
  - [l'USART](#lusart)
    - [Objectif du port série](#objectif-du-port-série)
    - [Comment nous l'avons implémenté](#comment-nous-lavons-implémenté)
- [Conclusion](#conclusion)

# État du projet

## Matériel utilisé
Le matéirel utilisé est:
- une carte STM32 Nucleo64
- une carte fille "SHIELD TP"

Les composant utilisé:
- Led RGB 
- bouton poussoir
- USART (port série)
- Interrupt controller

## Ce que l'on a réalisé
Lors de ce projet nous avons réalisé un Simon sur notre carte STM32.

### Générateur aléatoire
Pour ce faire nous avons dû réaliser dans un premier temps un générateur aléatoire afin de pouvoir générer des parties différentes à chaque fois. Et ce car nous ne pouvions pas utiliser la bibliothèque standard math.h. donc pour ce faire nous avons intégré l'algorithme nommé **Standard minimal** dans une bibliothèque *generateur_aleatoire.c*

### Initialisation du germe
Afin de finalement avoir un vrai générateur aléatoire il nous fallais générer un germe potentiellement toujours différent à chaque nouveaux lancement de la carte. Pour réaliser cela nous avons décidé de faire appuyer un bouton pressoir à l'utilisateur. Nous retenons le temps que l'utilisateur à appuyer sur le bouton en milli seconde. C'est ce temps qui sert de germe à notre générateur aléatoire.

### paramétrage des composants
Nous avons eu des difficultés à paramétrer la LED RGB qui est dans la fonction init_LD3(), car nous n'avions pas la documentation de la carte fille pour comprendre à quel pin GPIO elle était branchée.
Pour les autres composants nous avons utilisé les fonctions déjà implémenter, en modifiant le pin pour le bouton poussoir. 

## Ce que l'on a abandonné

### Ajout de plus de couleurs 
Ce que nous avons abandonné est d'utiliser plus de couleur en combinant par exemple le rouge et le bleu pour faire du magenta car le résultat était décevant. On ne percevait pas clairement le mélange des couleurs.

### Ajout de son
Ce que nous avons abandonné est l'utilisation du buzzer pour animer la partie car le son émis était vraiment faible.

## Ce que l'on aurait aimé implenter

### Ajout d'un mode multijoueur
Avec les 4 interrupteurs nous aurions pu en sélectionner deux pour mettre un nombre de joueurs de 1 à 4 joueurs, chaque joueur pourrait jouer tour à tour et serait éliminer lorsqu'il fait une erreur.
Il suffirait juste de rajouter 4 variables qui correspondrait à l'état des 4 joueurs en jeu ou éliminer.

### Ajout d'un timer pour le temps de réponse
Nous aurions aimé implémenter est un timer afin que l'utilisateur est un temps restreint pour répondre. Pour faire cela on pourrait utiliser les interruptions et décider d'un temps pour répondre.

### Ajout d'un mode de difficulté
Avec deux interrupteurs nous aurions pu créer quatre modes de difficultés . Les paramètres qui pourront influencer la difficulté sont le temps de réponse, le temps de clignotement de chaque LED.

# Points important de notre projet

## Les interruptions

### Objectif des interruptions système

Le projet utilise des interruptions pour détecter l'appui d'un bouton poussoir et lancer ou interrompre une partie. la fréquence des interruptions est de 1000 hz.

### Comment nous avons réalisé les interruptions système

2 variables globales volatiles entières : state et exit

il existe 3 états:
- state 0 le jeu n'est pas lancer, si appuie sur le bouton passe en state 2
- state 1 la partie est en cours lors de l'appuie du bouton passe en state 0 et interrompt la partie et modifie exit
- state 2 lance un compteur N pour créer l'aléatoire et lors du relâchement du bouton passe en state 1 et commence la partie


## l'USART

### Objectif du port série
Le projet utilise le port série pour récupérer les numéros entrés par l'utilisateur, mais aussi pour envoyer des informations à l'utilisateur sur l'état de la partie

### Comment nous l'avons implémenté
Nous avons utilisé _getc() pour récupérer les informations tapées par l'utilisateur.
Nous avons utilisé _puts() pour l'envoi des règles et le score.
Pour le score nous avons dû créer une fonction parseur qui convertit un entier en une suite de caractères.

# Conclusion

La création de ce Simon sur la carte STM32 nous a permis de nous améliorer quant à la programmation système (les masques AND/OR) et a alors utilisations dans un projet concret. De plus, grâce à ce projet nous avons compris l'importance de la documentation, lors du processus de développement. Enfin ce projet a permis d'améliorer notre organisation et notre efficacité, en anticipant la majorité des fonctions et en se partageant au mieux le travail.


