#ifndef GEN_H
#define GEN_H

typedef unsigned long RAND_T;

void srand(RAND_T seed);

RAND_T rand();


#endif