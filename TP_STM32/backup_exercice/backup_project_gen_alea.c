
/*=======================================================*/
//   Générateur aléatoire simple pour carte embarqué     //
//   @author Alexandre Lithaud @author Corentin Thomazo  //
/*=======================================================*/

#include "generateur_aleatoire.h"

#define A 16807
#define C 0
#define M 2147483647

static volatile RAND_T seed = 1;
RAND_T ValeurAleatoire = 0;

void srand(RAND_T seed_man){
    seed = seed_man;
}

RAND_T rand(){

    if(ValeurAleatoire == 0)
        ValeurAleatoire = (A * seed + C) % M;
    else
       ValeurAleatoire = (A * ValeurAleatoire + C) % M; 

    return ValeurAleatoire;
}