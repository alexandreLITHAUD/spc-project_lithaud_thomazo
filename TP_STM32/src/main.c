#include <stdio.h>
//#include <math.h>
#include <string.h>
#include "sys/cm4.h"
#include "sys/devices.h"
#include "sys/init.h"
#include "sys/clock.h"
#include "generateur_aleatoire.h"

#define SIZE 32
#define R 82
#define G 71
#define B 66

static volatile char c=0;
int nb;
static volatile unsigned long N;
static volatile int state;
static volatile int exit;
int TAB[SIZE];

void init_LD2(){
	RCC.AHB1ENR |= 0x01;
	GPIOA.MODER = (GPIOA.MODER & 0xFFFFF3FF) | 0x00000400;
	GPIOA.OTYPER &= ~(0x1<<5);
	GPIOA.OSPEEDR |= 0x03<<10;
	GPIOA.PUPDR &= 0xFFFFF3FF;
}

void init_LD3(){
	RCC.AHB1ENR |= 0x01;
	GPIOA.MODER = (GPIOA.MODER & 0xFFC0FFFF) | 0x00150000;
	GPIOA.OTYPER &= ~(0x7<<8);
	GPIOA.OSPEEDR |= 0x3F<<16;
	GPIOA.PUPDR &= 0xFFC0FFFF;
}

void init_PB(){
	RCC.AHB1ENR |= 0x02;
	GPIOB.MODER = (GPIOC.MODER & 0xFFFFCFFF) ;
}

void tempo_500ms(){
	volatile uint32_t duree;
	for (duree = 0; duree < 5600000 ; duree++){
		;
	}

}

void init_USART(){
	GPIOA.MODER = (GPIOA.MODER & 0xFFFFFF0F) | 0x000000A0;
	GPIOA.AFRL = (GPIOA.AFRL & 0xFFFF00FF) | 0x00007700;
	USART2.BRR = get_APB1CLK()/9600;
	USART2.CR3 = 0;
	USART2.CR2 = 0;
}

void _putc(char c){
	while( ((USART2.SR & 0x80) == 0));  
	USART2.DR = c;
}

void _puts(char *c){
	int len = strlen(c);
	for (int i=0;i<len;i++){
		_putc(c[i]);
	}
}

char _getc(){
	while (( (USART2.SR & 0x20) == 0)&&exit==0); 
	return USART2.DR;

}

void systick_init(uint32_t freq){
	uint32_t p = get_SYSCLK()/freq;
	SysTick.LOAD = (p-1) & 0x00FFFFFF;
	SysTick.VAL = 0;
	SysTick.CTRL |= 7;
}

void __attribute__((interrupt)) SysTick_Handler(){
	// pas de partie lancé
	if (state == 0){
		if((GPIOB.IDR & 0x00000100) == 0){
			state = 2;
		}
	}
	// compte le temps où l'on appuie sur le bouton lorsque la partie se lance 
	if (state == 2){
		if((GPIOB.IDR & 0x00000100) == 0){
			N = (N+1) % 2147483647;
		}else{
			state = 1;
		}
	}
	//partie lancé
	if (state ==1 ){
		if((GPIOB.IDR& 0x00000100)==0){
			char* str	=	"merci d'avoir jouer!";
			_puts(str);
			_putc(0x0A);
			_putc(0x0D);
			char* str1	=	"Pour relancer réappuyer sur le bouton";
			_puts(str1);	
			_putc(0x0A);
			_putc(0x0D);
			exit	=	1;
			state	=	0;
		}
	}

}

// affiche un nombre en port série à partir d'un entier
void parseur(int score){
	char c;
	char* str = "le résultat est: ";
	_puts(str);
	if (score == 0){
		_putc((char)48);
	}
	while(score!=0){
		c = (score % 10) + 48;
		_putc(c);
		score = score / 10;
	}
}

void initgame(){
	char c=0;
	for (int i = 0 ; i < SIZE ; i++){
		switch(rand()%3){
			case 0:
				TAB[i] = R;
				//_putc(R); affiche le tableau de résultat pour le debug
				break;
			case 1:
				TAB[i] = G;
				//_putc(G); affiche le tableau de résultat pour le debug
				break;
			case 2:
				TAB[i] = B;
				//_putc(B); affiche le tableau de résultat pour le debug
				break;
		}
	}
	_putc(0x0A);
	_putc(0x0D);
}
void lecture(int score){
	for(int i = 0 ; i < score+1 ; i++){
		switch(TAB[i]){
			case R:
				GPIOA.ODR |= 0x00000100;
				tempo_500ms();
				GPIOA.ODR &= 0xFFFFFEFF;
				tempo_500ms();
				break;
			case G:
				GPIOA.ODR |= 0x00000200;
				tempo_500ms();
				GPIOA.ODR &= 0xFFFFFDFF;
				tempo_500ms();
				break;
			case B:
				GPIOA.ODR |= 0x00000400;
				tempo_500ms();
				GPIOA.ODR &= 0xFFFFFBFF;
				tempo_500ms();
				break;
				break;

		}
	}
}
int reception_joueur(int score){
		char c;
		char* str = "votre proposition est :";
		puts(str);
		//_putc(0x0A);
		_putc(0x0D);
		for(int i = 0 ; i < score+1 ; i++){
			if(exit){
				return 0;
			}
			c = _getc();
			// permet l'affichage de ce que l'on a tapé  pour le debug
			// à enlever pour la mise en prod
			_putc(c);
			if ((int)c != TAB[i]){
				return 0;
			}
		}
		return 1;

}


int jouer_coup(int score){
	lecture(score);
	return reception_joueur(score);
}

void party(){
	int score = 0;
	int win = 1;
	initgame();
	while(win && score < SIZE){
		win = jouer_coup(score);
		_putc(0x0A);
		_putc(0x0D);
		//temps entre chaque coup 1 s
		tempo_500ms();
		tempo_500ms();
		score++;
	}
	
	if (score == SIZE){
		char * str = "Vous avez gagné, félicitation!";
		_puts(str);
		_putc(0x0A);
		_putc(0x0D);
	}else{
		char * str = "Vous avez perdu, dommage...";
		_puts(str);
		_putc(0x0A);
		_putc(0x0D);
	}
	parseur(score);
}

int main() {
  
	printf("\e[2J\e[1;1H\r\n");
	printf("\e[01;32m*** Welcome to Nucleo F446 ! ***\e[00m\r\n");

	printf("\e[01;31m\t%08lx-%08lx-%08lx\e[00m\r\n",
	       U_ID[0],U_ID[1],U_ID[2]);
	printf("SYSCLK = %9lu Hz\r\n",get_SYSCLK());
	printf("AHBCLK = %9lu Hz\r\n",get_AHBCLK());
	printf("APB1CLK= %9lu Hz\r\n",get_APB1CLK());
	printf("APB2CLK= %9lu Hz\r\n",get_APB2CLK());
	printf("\r\n");

	init_LD2();
	init_LD3();
	init_PB();
	init_USART();
	systick_init(1000);
	//GPIOA.ODR &= ~0x00000020;
	GPIOA.ODR &= 0xFFFF0000;
	state  = 0;
	nb = 0;
	N = 0;
	exit = 0;

	char* str = "Bienvenue dans le jeu du Simon !";
	puts(str);
	_putc(0x0A);
	_putc(0x0D);
	char* str2 = "Vous aurez à répondre avec : R pour rouge ; G pour vert ; et B pour bleu !";
	puts(str2);
	_putc(0x0A);
	_putc(0x0D);
	char* str3 = "Appuyer sur le bouton pour commencer !";
	puts(str3);
	_putc(0x0A);
	_putc(0x0D);
	char* str4 = "Quand la partie est lancé appuyer sur le bouton pour la quitter !";
	puts(str4);
	_putc(0x0A);
	_putc(0x0D);

	while (1){
		if(state == 1){
			srand(N);
			_putc(0x0A);
			_putc(0x0D);
			exit = 0;
			party();
			state = 0;
			N = 0;
		}
	}
}
